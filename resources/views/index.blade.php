<!doctype html>
<html>
    <head>
        <title>Bachelorarbeit</title>
        <link rel="stylesheet" type="text/css" href="{{ asset("css/app.css") }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

    <body>
        <div id="app">
            <app></app>
        </div>
    </body>

    <script src="{{ asset("js/app.js") }}"></script>
</html>