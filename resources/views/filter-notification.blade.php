<p>

Sehr geehrte/r {{ $user->name }} <br> <br>

für deinen Filter Filtername, gibt es folgende neue Themen: <br> <br>

@foreach($topics as $key => $topic)
    Thema {{ $key + 1 }}: {{ $topic->name }} (<a href="mailto:{{ $topic->contact->email }}">{{ $topic->contact->name }}</a>) <br>
@endforeach

<br>

Mit freundlichen Grüßen <br>
Dein ARK Team

</p>