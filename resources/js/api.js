import axios from 'axios';

const api = axios.create({baseURL: '', withCredentials: false});

api.interceptors.request.use(function (config) {
    const access_token = localStorage.getItem('access_token');

    if (access_token) {
        config.headers.Authorization = 'Bearer ' + access_token;
    }

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
api.interceptors.response.use(
    function (response) {
        return response;
    }, 
  
    function (error) {
        const errorReponse = error.response.data.message;

        if (errorReponse === 'Token has expired') {
            refreshAccessToken(error);
        }

        console.log(error);
        
        return Promise.reject(error);
    }
);

let callbackQue = [];
let refreshAccessTokenLocked = false;

async function refreshAccessToken(error) {
    try {
        const { response: errorReponse } = error;
        const retry = new Promise(resolve => {
            callbackQue.push((accessToken) => {
                errorReponse.config.headers.Authorization = 'Bearer ' + accessToken;
                resolve(api(errorReponse.config));
            })
        });

        if (! refreshAccessTokenLocked) {
            refreshAccessTokenLocked = true;
            api.post('/refresh')
                .then(response => {
                    localStorage.setItem('user', JSON.stringify(response.data.user));
                    localStorage.setItem('access_token', response.data.access_token);

                    callbackQue.forEach(callback => callback(response.data.access_token))
                    callbackQue = [];
                })
                .catch(error => {
                    return Promise.reject(error);
                })
        }

        return retry;
    }catch(error) {
        return await Promise.reject(error);
    }
}

export default api;