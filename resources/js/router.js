import VueRouter from 'vue-router';

import FAQ from './pages/FAQ.vue';
import Home from './pages/Home.vue';
import Login from './pages/Login.vue';
import Registration from './pages/Registration.vue';
import Profile from './pages/Profile.vue';
import Error from './pages/Error.vue';
import Contact from './pages/Contact.vue';
import Archive from './pages/Archive.vue';
import MyTopics from './pages/MyTopics.vue';

const router = new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            },
        },

        {
            path: '/login',
            name: 'login',
            meta: {
                requiresAuth: false
            },
            component: Login
        },

        {
            path: '/register',
            name: 'register',
            meta: {
                requiresAuth: false
            },
            component: Registration
        },

        {
            path: '/archive',
            name: 'archive',
            meta: {
                requiresAuth: true
            },
            component: Archive
        },

        {
            path: '/profile',
            name: 'profile',
            meta: {
                requiresAuth: true
            },
            component: Profile
        },

        {
            path: '/faq',
            name: 'FAQ',
            meta: {
                requiresAuth: true
            },
            component: FAQ
        },

        {
            path: '/contact',
            name: 'contact',
            meta: {
                requiresAuth: true
            },
            component: Contact
        },

        {
            path: '/mytopics',
            name: 'mytopics',
            meta: {
                requiresAuth: true
            },
            component: MyTopics
        },

        { 
            path: "*", 
            component: Error,
            meta: {
                requiresAuth: false
            }, 
        }
    ]
})


router.beforeEach((to, from, next) => {
    const access_token = localStorage.getItem('access_token');

    if(to.meta.requiresAuth && !access_token) {
        next({name: 'login'});            
    }

    next();
})

export default router;