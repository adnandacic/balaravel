-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bachelorarbeit
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'student'),(3,'supervisor');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic_types`
--

DROP TABLE IF EXISTS `topic_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topic_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic_types`
--

LOCK TABLES `topic_types` WRITE;
/*!40000 ALTER TABLE `topic_types` DISABLE KEYS */;
INSERT INTO `topic_types` VALUES (1,'Bachelor'),(2,'Master'),(3,'Seminar');
/*!40000 ALTER TABLE `topic_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(191) NOT NULL,
  `type_id` int(10) unsigned NOT NULL DEFAULT '1',
  `file` varchar(191) NOT NULL,
  `work_group_id` int(10) unsigned NOT NULL DEFAULT '1',
  `language` enum('en','de') NOT NULL DEFAULT 'de',
  `abstract` text,
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `topics_user_id_foreign` (`user_id`),
  KEY `topics_work_group_id_foreign_idx` (`work_group_id`),
  KEY `topics_type_id_foreign_idx` (`type_id`),
  FULLTEXT KEY `name` (`name`,`abstract`),
  FULLTEXT KEY `abstract` (`abstract`),
  CONSTRAINT `topics_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `topic_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `topics_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `topics_work_group_id_foreign` FOREIGN KEY (`work_group_id`) REFERENCES `work_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topics`
--

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` VALUES (1,1,'Spatio-textual Data Generator test4',1,'topic1.pdf',1,'en','Over the last decade data are becoming increasingly more complex, as objects can be easily `tagged’ with different types of auxiliary information. The proliferation of smartphones or location-aware devices in general, and the unprecedented rise of social networks have resulted in massive amounts of spatially enhanced text data. At the same time, spatio-textual queries are being supported in real-life applications, such as Google Maps, Foursquare and Twitter. etestsds test',0),(2,1,'GeoSocial Reachability',1,'topic2.pdf',1,'en','Reachability is a fundamental problem in directed graphs; given two nodes A and B, the queries are being supported in real-life applications, such as Google Maps',1),(3,4,'Route Inference',1,'topic3.pdf',1,'en','Shortest path search is the core component of distance-based queries in graphs. Routing',0),(4,4,'Spatio-Textual Outlier Detection',1,'topic4.pdf',1,'en','Outlier detection is a popular and important data mining operation; the goal is to identify',0),(5,4,'Alternative Routing (1)',1,'topic5.pdf',1,'en','Shortest path search is the core component of distance-based queries in graphs. Routing',0),(6,3,'Alternative Routing (2)',1,'topic6.pdf',2,'en','Shortest path search is the core component of distance-based queries in graphs. Routing foo',0),(7,3,'Topic7',2,'dummy.pdf',2,'en','Lorem Ipsum 7',0),(8,3,'Topic8',2,'dummy.pdf',2,'de','Lorem Ipsum Foo 8 foo',0),(9,3,'Topic9',2,'dummy.pdf',2,'de','Lorem Ipsum Foo 9 foo',0),(10,3,'Topic10',2,'dummy.pdf',3,'de','Lorem Ipsum Foo 10 foo',0),(11,3,'Topic11',2,'dummy.pdf',3,'de','Lorem Ipsum Foo 11',0),(12,3,'Topic12',3,'dummy.pdf',3,'de','Lorem Ipsum Foo 12',0),(13,3,'Topic13',1,'dummy.pdf',3,'de','Lorem Ipsum Foo 13',0),(14,3,'Topic14',1,'dummy.pdf',3,'de','Lorem Ipsum Foo 14',0),(15,3,'Topic15',1,'dummy.pdf',4,'de','Lorem Ipsum Foo 15',0),(16,3,'Topic16',1,'dummy.pdf',4,'de','Lorem Ipsum Foo 16',0),(17,2,'Topic17',1,'dummy.pdf',4,'de','Lorem Ipsum Foo 17',0),(18,2,'Topic18',1,'dummy.pdf',5,'de','Lorem Ipsum Foo 18',0),(19,2,'Topic19',1,'dummy.pdf',5,'de','Lorem Ipsum Foo 19',1),(20,2,'Topic 20',1,'dummy.pdf',6,'de','Lorem Ipsum Foo 20',1),(21,1,'Test test test1',1,'W14uv3jkSlIBGma6a9x9hLo0aW8AlXlMcvX7xP2B.pdf',5,'de','Test test test',0),(22,1,'tetetetesds',1,'3NHTAiHokobJxjUd4TdV0n8UlOa93S8QyJzZFT1l.pdf',1,'de','teateaeata',0);
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_filters`
--

DROP TABLE IF EXISTS `user_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `filters` json NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_filters_user_id_foreign_idx` (`user_id`),
  CONSTRAINT `user_filters_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_filters`
--

LOCK TABLES `user_filters` WRITE;
/*!40000 ALTER TABLE `user_filters` DISABLE KEYS */;
INSERT INTO `user_filters` VALUES (1,1,'{\"type_id\": \"1\", \"language\": \"de\", \"work_groups\": [1, 2]}','Test'),(2,1,'{\"type_id\": null, \"language\": null, \"work_groups\": []}','Test123'),(6,1,'{\"type_id\": 0, \"language\": 0, \"work_groups\": [1, 2, 3, 4, 5]}','foo');
/*!40000 ALTER TABLE `user_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_notifications`
--

DROP TABLE IF EXISTS `user_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `filter_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_notifications`
--

LOCK TABLES `user_notifications` WRITE;
/*!40000 ALTER TABLE `user_notifications` DISABLE KEYS */;
INSERT INTO `user_notifications` VALUES (1,1,6);
/*!40000 ALTER TABLE `user_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `users_role_id_foreign_idx` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,3,'Adnan Dacic','adacic@students.uni-mainz.de','$2y$10$WdQ/wiLofXOnTNrRCrNdMenZL4ekVJPKZJJrTB3Vv9MiA3f.1SjSO',NULL),(2,1,'Mahir Bahtijarevic','mbahti@students.uni-mainz.de','',NULL),(3,3,'Hans-Jürgen Schröder','schroeder@uni-mainz.de','',NULL),(4,3,'Panagiotis Bouros','bouros@uni-mainz.de','',NULL),(5,1,'John Doe','john@gmail.com','$2y$10$Utled8JlmQoM6LMTbDAwYOLLMbZX2896h0.gVig.aCn.2JTtNhhgu',NULL),(6,1,'Test Testerino','tester@students.uni-mainz.de','$2y$10$xnPsDOShWWz4eILhhBGdG.rs1hrdxV9AGrM7arRpRfMRWr9l3LIn.',NULL),(7,1,'Professor Tester','prof@uni-mainz.de','$2y$10$iE.SkEtyny2u9/82g17WdOdbNza8Sxh4u4mnyevP5l4ARoujYcati',NULL),(8,1,'test','test1@students.uni-mainz.de','$2y$10$dYgzmc//kqmld1yZLTkV..7FIiUp6Nkl036H7ccxMHmUCfqDUpFpu',NULL),(9,3,'test2','test2@uni-mainz.de','$2y$10$OtV.Q4m9DViKUc1ESje/b.iQx46clc6dNg6ORMZVqhpBfRSHdjJVK',NULL),(10,2,'test3','test3@students.uni-mainz.de','$2y$10$BEBue9UporUS1GG7qr7Li.1XUzmEUzRfp/48tKMhr/LAs5oTQ9hee',NULL),(11,2,'tester','testfoobar@students.uni-mainz.de','$2y$10$Dly1OWkUfuiTfosVWP75V.kN4dy5mOER3V61Evhn/yadmlgz9IirC',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_groups`
--

DROP TABLE IF EXISTS `work_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_groups`
--

LOCK TABLES `work_groups` WRITE;
/*!40000 ALTER TABLE `work_groups` DISABLE KEYS */;
INSERT INTO `work_groups` VALUES (1,'Algorithmics'),(2,'Computational Geometry'),(3,'Data Management'),(4,'Data Mining'),(5,'Didaktik der Informatik'),(6,'Efficient Computing and Storage Systems'),(7,'High Perfomance Computing'),(8,'Programming Languages'),(9,'Scientific Computing and Bioinformatics'),(10,'Sportinformatik'),(11,'Visual Computing');
/*!40000 ALTER TABLE `work_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-22  1:18:40
