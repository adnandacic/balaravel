<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\WorkGroupController;
use App\Http\Controllers\TopicTypeController;
use App\Http\Controllers\UserFilterController;
use App\Http\Controllers\UserNotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'auth:api',
], function($router) {
    Route::group([
        'prefix' => 'auth'
    ], function ($router) {
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/refresh', [AuthController::class, 'refresh']);
        Route::get('/user-profile', [AuthController::class, 'userProfile']);    
    });

    Route::get('topics', [TopicController::class, 'index']);
    Route::get('user-topics', [TopicController::class, 'getUserTopics']);
    Route::post('topics', [TopicController::class, 'store']);
    Route::patch('topics/{topic}', [TopicController::class, 'update']);
    Route::patch('topics/{topic}/archive', [TopicController::class, 'archive']);
    Route::delete('topics/{topic}', [TopicController::class, 'destroy']);
    Route::get('work-groups', [WorkGroupController::class, 'index']);
    Route::get('topic-types', [TopicTypeController::class, 'index']);
    Route::post('user-filters', [UserFilterController::class, 'store']);
    Route::get('user-filters/{user}', [UserFilterController::class, 'index']);
    Route::delete('user-filters/{userFilters}', [UserFilterController::class, 'destroy']);
    Route::get('user-notification/{user}', [UserNotificationController::class, 'show']);
    Route::patch('user-notification/{user}', [UserNotificationController::class, 'update']);
});

Route::group([
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']); 
});
