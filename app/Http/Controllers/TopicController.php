<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\TopicType;

class TopicController extends Controller
{
    public function index(Request $request)
    {
        //N+1 problem
        // 1 topci query + 10 contact querys + 10 type querys

        //solultion je with (lazy load)
        // 1 topic query + contacy query + type query

        //good for max 1kish data
        //bad for big data

        //where in ([1k ids = slow])

        //fastet solution is to use JOINS

        //when returning data from controller, 
        //data automatically gets converted to JSON

        //TODO: implement search filters and profile
        
        return Topic::with(['contact', 'type'])
            ->topicType($request->get('type_id'))
            ->language($request->get('language'))
            ->workGroups($request->get('work_groups'))
            ->search($request->get('search'))
            ->archived($request->get('archived'))
            ->paginate(10);
    }

    public function getUserTopics(Request $request)
    {
        return Topic::with('type')
            ->user($request->get('user_id'))
            ->archived($request->get('archived'))
            ->get();
    }

    public function store(Request $request)
    {
        Topic::create($request->all());

        return response()->noContent();
    }

    public function update(Topic $topic, Request $request)
    {
        $topic->update($data = $request->only([
            'type_id', 'name', 'work_group_id', 'language', 'abstract', 'file'
        ]));

        return Topic::where('id', $topic->id)->with('type')->first();
    }

    public function archive(Topic $topic, Request $request)
    {
        $topic->update($request->only('archived'));
        
        return response()->noContent();
    }

    public function destroy(Topic $topic)
    {
        $topic->delete();
        
        return response()->noContent();
    }
}
