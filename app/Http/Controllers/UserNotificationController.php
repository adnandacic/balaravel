<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Http\Request;

class UserNotificationController extends Controller
{
    public function show(User $user)
    {
        return $user->notification;
    }

    public function update(Request $request, User $user)
    {
        UserNotification::updateOrCreate(
            ['user_id' => $user->id],
            ['filter_id' => $request->filter_id]
        );

        return response()->noContent();
    }
}
