<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TopicType;

class TopicTypeController extends Controller
{
    public function index()
    {
        return TopicType::all();
    }
}