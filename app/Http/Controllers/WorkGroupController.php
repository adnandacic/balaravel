<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WorkGroup;

class WorkGroupController extends Controller
{
    public function index()
    {
        return WorkGroup::all();
    }
}
