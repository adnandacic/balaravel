<?php

namespace App\Http\Controllers;

use App\Models\UserFilter;
use App\Models\User;
use Illuminate\Http\Request;

class UserFilterController extends Controller
{
    public function index(User $user)
    {
       return $user->filters;
    }

    public function store(Request $request)
    {
        return UserFilter::create($request->only(['user_id', 'filters', 'name']));
    }

    public function destroy(UserFilter $userFilters)
    {
        $userFilters->delete();
        return response()->noContent();
    }
}
