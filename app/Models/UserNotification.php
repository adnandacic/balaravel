<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = ['id'];

    public function filter() 
    {
        return $this->belongsTo(UserFilter::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
