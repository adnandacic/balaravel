<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'archived' => 'boolean',
    ];

    public $timestamps = false; //Disables created_at and updated_at field update on create or update

    public static function boot()
    {
        parent::boot();

        static::deleting(function($topic) {
            \Storage::disk('files')->delete($topic->file);
        });
    }

    public function contact()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function type()
    {
        return $this->belongsTo(TopicType::class);
    }

    public function scopeTopicType($query, $type)
    {
        if (! $type) {
            return $query;
        }

        return $query->where('type_id', $type);
    }

    public function scopeLanguage($query, $language) 
    {
        if (! $language) {
            return $query;
        }

        return $query->where('language', $language);
    }

    public function scopeWorkGroups($query, $workGroups) 
    {
        if (! $workGroups) {
            return $query;
        }

        return $query->whereIn('work_group_id', $workGroups);
    }

    public function scopeSearch($query, $terms) 
    {
        if (! $terms) {
            return $query;
        }

        $terms = str_replace(' ', ',', $terms);

        return $query->whereRaw("MATCH(name,abstract)AGAINST('$terms' IN NATURAL LANGUAGE MODE)");
    }

    public function scopeUser($query, $user_id)
    {
        if (! $user_id) {
            return $query;
        }

        return $query->where('user_id', $user_id);
    }

    public function scopeArchived($query, $archived)
    {
        if ($archived === null) {
            return $query;
        }

        return $query->where('archived', $archived);
    }

    public function setFileAttribute($file)
    {
        $this->attributes['file'] = is_string($file) ?
            $file :
            \Storage::disk('files')->put('/', $file);               
    }
}
