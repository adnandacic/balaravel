<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFilter extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'filters' => 'object'
    ];

    public function setFiltersAttribute($filters)
    {
        $this->attributes['filters'] = json_encode($filters);
    }
}
