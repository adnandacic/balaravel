<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\UserNotification;
use App\Models\Topic;

class FilterNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $topics;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($topics, $user)
    {
        $this->topics = $topics;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('info@ark.de') //fictive email address
            ->to($this->user->email)
            ->subject('Filter Notification')
            ->view('filter-notification');
    }
}
