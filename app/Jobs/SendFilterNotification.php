<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\UserNotification;
use App\Models\Topic;
use App\Mail\FilterNotificationMail;

class SendFilterNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        UserNotification::all()->each(function($userNotification) {
            $topics = $this->getTopics($userNotification->filter->filters);
  
            if ($topics->count()) {
                \Mail::send(new FilterNotificationMail(
                    $topics,
                    $userNotification->user
                ));
            }
        });
    }

    public function getTopics($filters)
    {
        return Topic::with('contact')
            ->topicType($filters->type_id)
            ->language($filters->language)
            ->workGroups($filters->work_groups)
            ->archived(0)
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
    }
}
